var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    recipient: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    status: {
        type: 'number',
        required: true // 0 : new request , 1: accepted
    },
    created_at: {
        type: Date,
        required: false
    }
});

module.exports = mongoose.model('Friend_Request', schema);

