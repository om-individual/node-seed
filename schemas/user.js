var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    email: {
        type: String,
        required: false,
        default: null
    },
    password: {
        type: String,
        required: true
    },
    first_name: {
        type: String,
        required: false,
        default: null
    },
    last_name: {
        type: String,
        required: false,
        default: null
    },
    phone_number: {
        type: String,
        required: false,
        default: null
    },
    gender: {
        type: Number, // 0 : male , 1 : female
        required: false,
        default: 0
    },
    birthday: {
        type: Date,
        required: false
    },
    address: {
        type: String,
        required: false,
        default: null
    },
    religion: {
        type: String,
        required: false,
        default: null
    },
    intro: { //  description
        type: String,
        required: false,
        default: null
    },
    fb_id: {
        type: String,
        required: false,
        default: null
    },
    avatar: {
        type: String,
        required: false,
        default: null
    },
    cover: {
        type: String,
        required: false,
        default: null
    },
    created_at: {
        type: Date,
        required: false
    },
    last_visited_at: {
        type: Date,
        required: false
    }
});

module.exports = mongoose.model('User', schema);
