var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    user_one: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    user_two: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    created_at: {
        type: Date,
        required: false
    }
});

module.exports = mongoose.model('Friend', schema);

